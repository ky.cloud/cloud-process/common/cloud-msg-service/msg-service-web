# msg-service-web （消息服务平台）

## 对外服务
## 1、发送邮件
| 内容    | 方式           |
|-------|--------------|
| 请求方式  | rocketmq     |
| topic | msg          |
| tag   | send_msg     |
| 消息体   | MsgSendMqDTO |
### MsgSendMqDTO
| 字段           | 描述   | 类型              | 必传  | 说明                                                        |
|--------------|------|-----------------|-----|-----------------------------------------------------------|
| templateCode | 模板编码 | String          | 是   | 申请的消息模板编码                                                 |
| toUser       | 发送人  | List&lt;String> | 否   | <br>当发送消息类型为短信,此为接收人手机号;<br/><br>当发送消息类型为邮件,此为接收人邮箱;<br/> |




