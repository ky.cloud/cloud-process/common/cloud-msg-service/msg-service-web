#基础镜像
FROM openjdk:8
# 挂载点
VOLUME /tmp
ADD target/app.jar app.jar
# 定义容器启动时执行的命令
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN echo 'Asia/Shanghai' >/etc/timezone
ENTRYPOINT ["java","-Dspring.profiles.active=test","-jar","/app.jar"]


