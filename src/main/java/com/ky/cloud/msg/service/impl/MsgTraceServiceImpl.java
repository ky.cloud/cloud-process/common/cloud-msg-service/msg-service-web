package com.ky.cloud.msg.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.ky.cloud.components.restful.model.PageQuery;
import com.ky.cloud.msg.entity.MsgTrace;
import com.ky.cloud.msg.mapper.MsgTraceMapper;
import com.ky.cloud.msg.service.MsgTraceService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消息轨迹表 服务实现类
 * </p>
 *
 * @author Administrator
 * @since 2023-05-18
 */
@Service
public class MsgTraceServiceImpl extends ServiceImpl<MsgTraceMapper, MsgTrace> implements MsgTraceService {

    @Override
    public Page<MsgTrace> pageQuery(MsgTrace msgTrace, PageQuery pageQuery) {
        QueryWrapper<MsgTrace> queryWrapper = new QueryWrapper<>(msgTrace);
        return PageHelper.startPage(pageQuery.getPageIndex(), pageQuery.getPageSize()).doSelectPage(() -> list(queryWrapper));
    }
}
