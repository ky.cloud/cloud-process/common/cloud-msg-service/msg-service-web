package com.ky.cloud.msg.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.ky.cloud.components.restful.model.PageQuery;
import com.ky.cloud.msg.entity.MsgConfig;
import com.ky.cloud.msg.mapper.MsgConfigMapper;
import com.ky.cloud.msg.service.MsgConfigService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 消息配置表 服务实现类
 * </p>
 *
 * @author Administrator
 * @since 2023-05-30
 */
@Service
public class MsgConfigServiceImpl extends ServiceImpl<MsgConfigMapper, MsgConfig> implements MsgConfigService {


    @Override
    public Page<MsgConfig> pageQuery(MsgConfig msgConfig, PageQuery pageQuery) {
        msgConfig.setIsDelete(false);
        QueryWrapper<MsgConfig> queryWrapper = new QueryWrapper<>(msgConfig);
        return PageHelper.startPage(pageQuery.getPageIndex(), pageQuery.getPageSize()).doSelectPage(() -> list(queryWrapper));
    }

    @Override
    public MsgConfig getMsgConfigCache(String msgCode) {
        QueryWrapper<MsgConfig> msgConfigWrapper = new QueryWrapper<>();
        msgConfigWrapper.lambda().eq(MsgConfig::getMsgCode, msgCode);
        return baseMapper.selectOne(msgConfigWrapper);
    }
}
