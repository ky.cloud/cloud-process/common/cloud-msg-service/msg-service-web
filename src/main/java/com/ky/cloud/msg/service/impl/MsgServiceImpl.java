package com.ky.cloud.msg.service.impl;

import com.ky.cloud.msg.entity.MsgConfig;
import com.ky.cloud.msg.enums.MsgTypeEnum;
import com.ky.cloud.msg.exception.MsgException;
import com.ky.cloud.msg.mq.receive.MsgSendMqDTO;
import com.ky.cloud.msg.service.MsgService;
import com.ky.cloud.msg.utils.MailUtil;
import com.ky.cloud.msg.utils.SMSUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 消息服务接口实现
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Service
public class MsgServiceImpl implements MsgService {
    @Resource
    private SMSUtil smsUtil;
    @Resource
    private MailUtil mailUtil;

    @Override
    public boolean sendMsg(MsgSendMqDTO msgSendMqDTO) throws MsgException {
        MsgConfig msgConfig = msgSendMqDTO.getMsgConfig();
        if (msgConfig.getType().equals(MsgTypeEnum.SMS.getType())) {
            return smsUtil.sendSms(msgSendMqDTO);
        } else if (msgConfig.getType().equals(MsgTypeEnum.MAIL.getType())) {
            return mailUtil.sendMail(msgSendMqDTO);
        } else {
            throw new MsgException("暂不支持此消息类型");
        }
    }
}
