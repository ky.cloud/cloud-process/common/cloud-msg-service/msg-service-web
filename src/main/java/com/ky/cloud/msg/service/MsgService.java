package com.ky.cloud.msg.service;

import com.ky.cloud.msg.exception.MsgException;
import com.ky.cloud.msg.mq.receive.MsgSendMqDTO;

/**
 * 消息服务接口
 *
 * @author chenHui
 * @version 1.0.0
 */
public interface MsgService {
    /**
     * 发送消息
     * 
     * @param msgSendMqDTO MsgSendDTO
     * @return boolean
     * @since 1.0.0
     */
    boolean sendMsg(MsgSendMqDTO msgSendMqDTO) throws MsgException;

}
