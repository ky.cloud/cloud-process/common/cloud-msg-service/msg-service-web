package com.ky.cloud.msg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.Page;
import com.ky.cloud.components.restful.model.PageQuery;
import com.ky.cloud.msg.entity.MsgTrace;

/**
 * <p>
 * 消息轨迹表 服务类
 * </p>
 *
 * @author Administrator
 * @since 2023-05-18
 */
public interface MsgTraceService extends IService<MsgTrace> {
    /**
     * 分页查询
     *
     * @param msgTrace Trace
     * @param pageQuery PageQuery
     * @return PageInfo<Template>
     * @since 1.0.0
     */
    Page<MsgTrace> pageQuery(MsgTrace msgTrace, PageQuery pageQuery);
}
