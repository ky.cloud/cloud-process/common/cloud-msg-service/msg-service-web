package com.ky.cloud.msg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.Page;
import com.ky.cloud.components.restful.model.PageQuery;
import com.ky.cloud.msg.entity.MsgConfig;

/**
 * <p>
 * 消息配置表 服务类
 * </p>
 *
 * @author Administrator
 * @since 2023-05-30
 */
public interface MsgConfigService extends IService<MsgConfig> {
    /**
     * 分页查询
     *
     * @param msgConfig Config
     * @param pageQuery PageQuery
     * @return PageInfo<Template>
     * @since 1.0.0
     */
    Page<MsgConfig> pageQuery(MsgConfig msgConfig, PageQuery pageQuery);

    /**
     * 获取模板数据
     *
     * @param msgCode 模板消息
     * @return msgConfig
     * @since 1.0.0
     */
    MsgConfig getMsgConfigCache(String msgCode);
}
