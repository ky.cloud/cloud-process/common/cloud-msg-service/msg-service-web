package com.ky.cloud.msg.init;

import com.ky.cloud.components.redis.util.RedisUtils;
import com.ky.cloud.msg.entity.MsgConfig;
import com.ky.cloud.msg.mapper.MsgConfigMapper;
import io.lettuce.core.RedisException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 数据字典初始化缓存加载
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Component
public class InitDataCacheRunner implements CommandLineRunner {
    @Resource
    private MsgConfigMapper msgConfigMapper;
    @Resource
    private RedisUtils redisUtils;

    @Override
    public void run(String... args) {
        log.info("数据字典加载至redis开始执行");
        List<MsgConfig> msgConfigList = msgConfigMapper.selectMsgConfigLists();
        for (MsgConfig msgConfig : msgConfigList) {
            String msgCode = msgConfig.getMsgCode();
            MsgConfig msgConfigs = msgConfigMapper.selectMsgConfig(msgCode);
            if (!redisUtils.set(msgCode, msgConfigs)) {
                throw new RedisException("数据字典加载至redis异常");
            }
        }
        log.info("数据字典加载至redis执行结束");
    }
}
