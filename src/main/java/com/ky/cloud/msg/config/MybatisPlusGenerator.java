package com.ky.cloud.msg.config;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;
import com.ky.cloud.msg.entity.BaseEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author chenHui
 * @version 1.0.0
 */
public class MybatisPlusGenerator {


    public static void main(String[] args) {
        // 生成表
        List<String> tables = new ArrayList<>();
        tables.add("msg_trace");


        String url="jdbc:mysql://sh-cynosdbmysql-grp-rev1jryu.sql.tencentcdb.com:22045/mid-service";
        String username="root";
        String password="mysql_3306";

        String module="msg-service-web";
        String outputDir = System.getProperty("user.dir") + "\\" + module + "\\src\\main\\java";
        // 父包名称
        String parent="com.ky.cloud";
        // 父包模块名称
        String moduleName = "msg";

        List<String> commonEntityFields = new ArrayList<>();
        commonEntityFields.add("create_time");
        commonEntityFields.add("create_by");
        commonEntityFields.add("update_time");
        commonEntityFields.add("update_by");
        commonEntityFields.add("is_delete");

        FastAutoGenerator.create(url, username, password)
                // 全局配置
                .globalConfig(builder ->
                        builder.author("Administrator")
                                .outputDir(outputDir)
                                .dateType(DateType.ONLY_DATE))
                // 包配置
                .packageConfig(builder ->
                        builder.parent(parent)
                                .moduleName(moduleName)
                                .pathInfo(Collections.singletonMap(OutputFile.xml, System.getProperty("user.dir") + "\\" +
                                        module + "\\src\\main\\resources\\mapper")))
                // 策略配置
                .strategyConfig(builder ->
                        builder.addInclude(tables)
//                                .addTablePrefix(moduleName+"_")
                                // 开启生成实体类
                                .entityBuilder()
                                .enableLombok()
                                .superClass(BaseEntity.class)
                                .addSuperEntityColumns(commonEntityFields)
                                .enableFileOverride()
                                .naming(NamingStrategy.underline_to_camel)
                                // 开启字段注释
                                .enableTableFieldAnnotation()
                                .mapperBuilder()
                                .enableBaseResultMap()
                                .superClass(BaseMapper.class)
                                .formatMapperFileName("%sMapper")
                                .formatXmlFileName("%sMapper")
                                .serviceBuilder()
                                .formatServiceFileName("%sService")
                                .formatServiceImplFileName("%sServiceImpl"))
                .templateEngine(new VelocityTemplateEngine())
                // 关闭生成controller
                .templateConfig(builder -> builder.controller(""))
                .execute();

    }
}
