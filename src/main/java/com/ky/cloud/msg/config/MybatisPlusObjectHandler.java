package com.ky.cloud.msg.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.ky.cloud.components.auth.UserAuthUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;


/**
 * mybatis-plus对象自动填充值配置
 *
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Component
public class MybatisPlusObjectHandler implements MetaObjectHandler {

    @Resource
    private UserAuthUtils authUtils;

    @Override
    public void insertFill(MetaObject metaObject) {
        Date date = new Date();

        setFieldValByName("createTime", date, metaObject);
        setFieldValByName("updateTime", date, metaObject);

        String operator = authUtils.getUserNo();
        setFieldValByName("createBy", operator, metaObject);
        setFieldValByName("updateBy", operator, metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        Date date = new Date();
        setFieldValByName("updateTime", date, metaObject);
        setFieldValByName("updateBy", authUtils.getUserNo(), metaObject);
    }
}
