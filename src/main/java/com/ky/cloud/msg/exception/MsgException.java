package com.ky.cloud.msg.exception;

/**
 * @author chenHui
 * @version 1.0.0
 */
public class MsgException extends Exception{

    public MsgException(String message) {
        super(message);
    }
}
