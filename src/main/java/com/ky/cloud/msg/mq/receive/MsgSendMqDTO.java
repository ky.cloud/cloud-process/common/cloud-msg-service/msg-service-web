package com.ky.cloud.msg.mq.receive;

import com.alibaba.fastjson2.JSONObject;
import com.ky.cloud.msg.entity.MsgConfig;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 发送消息mqDTO
 *
 * @author chenHui
 * @version 1.0.0
 */
@Data
@Accessors(chain = true)
public class MsgSendMqDTO {
    /**
     * 消息模板id
     */
    private String msgCode;
    /**
     * 消息接收人
     */
    private List<String> toUser;
    /**
     * 抄送人员
     */
    private List<String> cc;

    /**
     * 模板动态参数
     */
    private JSONObject param;

    /**
     * 附件
     */
    private String attachment;

    /**
     * 系统id
     */
    private String appId;

    private String title;

    private String content;

    private String remark;

    private MsgConfig msgConfig;

}
