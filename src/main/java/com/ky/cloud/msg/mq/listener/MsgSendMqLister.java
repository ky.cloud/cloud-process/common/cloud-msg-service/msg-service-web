package com.ky.cloud.msg.mq.listener;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ky.cloud.msg.entity.MsgConfig;
import com.ky.cloud.msg.entity.MsgTrace;
import com.ky.cloud.msg.enums.MsgTypeEnum;
import com.ky.cloud.msg.enums.STATION_STATE_ENUM;
import com.ky.cloud.msg.exception.MsgException;
import com.ky.cloud.msg.mq.receive.MsgSendMqDTO;
import com.ky.cloud.msg.service.MsgConfigService;
import com.ky.cloud.msg.service.MsgService;
import com.ky.cloud.msg.service.MsgTraceService;
import com.ky.cloud.msg.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.Date;

/**
 * 发送消息
 *
 * @author chenHui
 * @version 1.0.0
 */
@Service
@Slf4j
@RocketMQMessageListener(topic = "${spring.profiles.active}_${rocketmq.producer.group}_msg", selectorExpression = "send_msg", consumerGroup = "${rocketmq.producer.group}_consumer")
public class MsgSendMqLister implements RocketMQListener<MessageExt> {
    @Resource
    private MsgService msgService;
    @Resource
    private MsgTraceService msgTraceService;
    @Resource
    private MsgConfigService msgConfigService;

    @Override
    public void onMessage(MessageExt messageExt) {
        String body = new String(messageExt.getBody(), StandardCharsets.UTF_8);
        MsgSendMqDTO msgSendMqDTO = JSONObject.parseObject(body, MsgSendMqDTO.class);

        //初始化轨迹对象
        MsgTrace msgTrace = new MsgTrace();
        msgTrace.setMsgData(JSON.toJSONString(body))
                .setMsgCode(msgSendMqDTO.getMsgCode())
                .setAppId(msgSendMqDTO.getAppId());
        //获取消息配置
        MsgConfig msgConfig = msgConfigService.getMsgConfigCache(msgSendMqDTO.getMsgCode());
        msgSendMqDTO.setMsgConfig(msgConfig);
        if (msgConfig == null) {
            log.error("消息编码:[{}]，未获取到消息配置数据", msgSendMqDTO.getMsgCode());
            msgTrace.setState("fail")
                    .setErrorMsg("未获取到消息处理模板");
            msgTraceService.save(msgTrace);
            return;
        }
        msgSendMqDTO.setTitle(msgConfig.getMsgTitle());

        if (msgConfig.getType().equals(MsgTypeEnum.STATION_MSG.getType())) {
            //替换模板参数
            String msgContent = msgConfig.getMsgContent();
            JSONObject param = msgSendMqDTO.getParam();
            String msg = StringUtils.replaceOfString(param, msgContent);
            msgTrace.setMsg(msg);
            msgTrace.setState(STATION_STATE_ENUM.UNREAD_STATE.getDesc())
                    .setSendTime(new Date());
        } else {
            // 获取消息模板配置
            try {
                if (msgService.sendMsg(msgSendMqDTO)) {
                    // 保存成功轨迹
                    msgTrace.setState("success")
                            .setSendTime(new Date());
                } else {
                    msgTrace.setState("fail")
                            .setErrorMsg("发送失败");
                }
            } catch (MsgException e) {
                msgTrace.setState("fail")
                        .setErrorMsg(e.getMessage());
            } catch (Exception e) {
                msgTrace.setState("fail")
                        .setErrorMsg("system error");
            }
        }
        msgTraceService.save(msgTrace);
    }
}
