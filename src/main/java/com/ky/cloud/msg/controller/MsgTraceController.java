package com.ky.cloud.msg.controller;

import com.github.pagehelper.Page;
import com.ky.cloud.components.restful.HttpResult;
import com.ky.cloud.components.restful.log.annotion.WebLog;
import com.ky.cloud.components.restful.model.PageQuery;
import com.ky.cloud.msg.entity.MsgTrace;
import com.ky.cloud.msg.service.MsgTraceService;
import com.ky.cloud.msg.utils.BeanCopyUtil;
import com.ky.cloud.msg.vo.req.MsgTraceReqVO;
import com.ky.cloud.msg.vo.res.MsgTraceResVO;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author chenhui_sinosoft
 * @version 1.0.0
 */
@RestController
@RequestMapping("/msgTrace")
public class MsgTraceController {
    @Resource
    private MsgTraceService msgTraceService;
    @WebLog
    @GetMapping()
    public HttpResult pageQuery(MsgTraceReqVO msgTraceReqVO, PageQuery pageQuery) {
        MsgTrace msgTrace = new MsgTrace();
        BeanUtils.copyProperties(msgTraceReqVO, msgTrace);
        Page<MsgTrace> page = msgTraceService.pageQuery(msgTrace, pageQuery);
        List<MsgTrace> pageResult = page.getResult();
        List<MsgTraceResVO> msgTraceResVOList = BeanCopyUtil.copyListProperties(pageResult, MsgTraceResVO::new);
        return HttpResult.success(msgTraceResVOList, page.getTotal(), page.getPages());
    }
}
