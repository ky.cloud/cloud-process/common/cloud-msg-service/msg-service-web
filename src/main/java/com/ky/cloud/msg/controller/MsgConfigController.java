package com.ky.cloud.msg.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.Page;
import com.ky.cloud.components.restful.HttpResult;
import com.ky.cloud.components.restful.log.annotion.WebLog;
import com.ky.cloud.components.restful.model.PageQuery;
import com.ky.cloud.components.restful.validated.annotation.ValidateGroup;
import com.ky.cloud.msg.entity.MsgConfig;
import com.ky.cloud.msg.enums.ConfigStateEnum;
import com.ky.cloud.msg.service.MsgConfigService;
import com.ky.cloud.msg.utils.BeanCopyUtil;
import com.ky.cloud.msg.vo.req.MsgConfigReqVO;
import com.ky.cloud.msg.vo.res.MsgConfigResVO;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author chenHui
 * @version 1.0.0
 */
@RequestMapping("/msgConfig")
@RestController
public class MsgConfigController {
    @Resource
    private MsgConfigService msgConfigService;
    @WebLog
    @GetMapping()
    public HttpResult pageQuery(MsgConfigReqVO msgConfigReqVO, PageQuery pageQuery) {
        MsgConfig msgConfig = new MsgConfig();
        BeanUtils.copyProperties(msgConfigReqVO, msgConfig);
        Page<MsgConfig> page = msgConfigService.pageQuery(msgConfig, pageQuery);
        List<MsgConfig> taskList = page.getResult();
        List<MsgConfigResVO> taskResVOList = BeanCopyUtil.copyListProperties(taskList, MsgConfigResVO::new);
        return HttpResult.success(taskResVOList, page.getTotal(), page.getPages());
    }


    @WebLog
    @PutMapping()
    public HttpResult edit(@RequestBody @Validated(ValidateGroup.UpdateCheck.class) MsgConfigReqVO msgConfigReqVO) {
        MsgConfig msgConfig = msgConfigService.getById(msgConfigReqVO.getMsgConfigId());
        if (!StringUtils.hasText(msgConfig.getMsgCode()) && !StringUtils.hasText(msgConfigReqVO.getMsgCode())) {
            return HttpResult.failure("请填写模板编码");
        }
        BeanUtils.copyProperties(msgConfigReqVO, msgConfig);
        // 更新数据
        if (!msgConfigService.updateById(msgConfig)) {
            return HttpResult.failure("操作失败，请重新尝试");
        }
        return HttpResult.success();
    }


    @WebLog
    @PostMapping()
    public HttpResult add(@RequestBody @Validated(ValidateGroup.InsertCheck.class) MsgConfigReqVO msgConfigReqVO) {
        if (StringUtils.hasText(msgConfigReqVO.getMsgCode())) {
            LambdaQueryWrapper<MsgConfig> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(MsgConfig::getMsgCode, msgConfigReqVO.getMsgCode()).eq(MsgConfig::getEvn, msgConfigReqVO.getEnv());
            if (msgConfigService.count(queryWrapper) > 0) {
                return HttpResult.failure(msgConfigReqVO.getEnv() + "环境此模板编码已经存在,请调整");
            }
        }
        MsgConfig msgConfig = new MsgConfig();
        msgConfig.setState(ConfigStateEnum.APPLY.getState());
        BeanUtils.copyProperties(msgConfigReqVO, msgConfig);
        if (!msgConfigService.save(msgConfig)) {
            return HttpResult.failure("新增失败，请重新尝试");
        }
        return HttpResult.success();
    }
}
