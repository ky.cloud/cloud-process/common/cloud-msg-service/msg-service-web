package com.ky.cloud.msg.mapper;

import com.ky.cloud.msg.entity.MsgTrace;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 消息轨迹表 Mapper 接口
 * </p>
 *
 * @author author
 * @since 2023-07-07
 */
public interface MsgTraceMapper extends BaseMapper<MsgTrace> {

}
