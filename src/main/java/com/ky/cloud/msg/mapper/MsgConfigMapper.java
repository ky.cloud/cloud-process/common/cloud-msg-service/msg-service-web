package com.ky.cloud.msg.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ky.cloud.msg.entity.MsgConfig;

import java.util.List;

/**
 * <p>
 * 消息配置表 Mapper 接口
 * </p>
 *
 * @author Administrator
 * @since 2023-06-09
 */
public interface MsgConfigMapper extends BaseMapper<MsgConfig> {
    /**
     * 查询所有未删除数据
     *
     * @return 数据集合
     */
    List<MsgConfig> selectMsgConfigLists();

    /**
     * 根据消息模板编码查询数据
     *
     * @param msgCode 消息编码
     * @return 数据
     */
    MsgConfig selectMsgConfig(String msgCode);
}
