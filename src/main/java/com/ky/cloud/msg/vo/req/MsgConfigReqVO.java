package com.ky.cloud.msg.vo.req;

import com.ky.cloud.components.restful.validated.annotation.ValidateGroup;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 工作任务查询入参VO
 *
 * @author chenHui
 * @version 1.0.0
 */
@Data
public class MsgConfigReqVO {

    /**
     * 消息主键
     */
    @NotNull(groups = {ValidateGroup.UpdateCheck.class,ValidateGroup.DeleteCheck.class}, message = "消息id不能为空")
    private Integer msgConfigId;

    /**
     * 消息编码
     */
    private String msgCode;

    /**
     * 消息类型
     */
    @NotBlank(groups = {ValidateGroup.InsertCheck.class}, message = "消息类型不能为空")
    private String type;

    /**
     * 模板内容
     */
    @NotBlank(groups = {ValidateGroup.InsertCheck.class}, message = "模板内容不能为空")
    private String content;

    /**
     * 模板格式
     */
    @NotBlank(groups = {ValidateGroup.InsertCheck.class}, message = "模板格式不能为空")
    private String style;

    /**
     * 应用环境
     */
    @NotBlank(groups = {ValidateGroup.InsertCheck.class}, message = "应用环境不能为空")
    private String env;

    /**
     * 系统id
     */
    @NotBlank(groups = {ValidateGroup.InsertCheck.class}, message = "应用系统不能为空")
    private String appId;

    /**
     * 状态
     */
    private String state;

    /**
     * 消息用途
     */
    @NotBlank(groups = {ValidateGroup.InsertCheck.class}, message = "消息用途不能为空")
    private String msgPurposes;
}
