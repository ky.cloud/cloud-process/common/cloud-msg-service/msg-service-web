package com.ky.cloud.msg.vo.res;


import lombok.Data;

/**
 * 工作任务查询入参VO
 *
 * @author chenHui
 * @version 1.0.0
 */
@Data
public class MsgConfigResVO {
    /**
     * 消息主键
     */
    private Integer templateId;

    /**
     * 消息类型
     */
    private String type;

    /**
     * 模板内容
     */
    private String content;

    /**
     * 模板格式
     */
    private String style;

    /**
     * 消息用途
     */
    private String msgPurposes;

    /**
     * 系统id
     */
    private String appId;

    /**
     * 状态
     */
    private String state;

    /**
     * 备注
     */
    private String remark;
}
