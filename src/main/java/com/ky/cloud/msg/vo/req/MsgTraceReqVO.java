package com.ky.cloud.msg.vo.req;

import lombok.Data;

import java.util.Date;

/**
 * @author chenhui_sinosoft
 * @version 1.0.0
 */
@Data
public class MsgTraceReqVO {
    /**
     * 模板id
     */
    private Integer templateId;

    /**
     * 系统id
     */
    private String appId;

    /**
     * 状态
     */
    private String state;

    /**
     * 发送日期
     */
    private Date sendDate;
}
