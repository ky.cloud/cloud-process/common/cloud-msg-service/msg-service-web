package com.ky.cloud.msg.vo.res;

import lombok.Data;

import java.util.Date;

/**
 * @author chenhui_sinosoft
 * @version 1.0.0
 */
@Data
public class MsgTraceResVO {
    /**
     * 轨迹id
     */
    private Integer traceId;

    /**
     * 消息数据
     */
    private String msgData;

    /**
     * 状态
     */
    private String state;

    /**
     * 错误信息
     */
    private String errorMsg;

    /**
     * 重试次数
     */
    private Integer retryCount;

    /**
     * 发送时间
     */
    private Date sendTime;

    /**
     * 系统id
     */
    private String appId;

    /**
     * 模板编码
     */
    private String templateCode;
}
