package com.ky.cloud.msg.dto;

import com.alibaba.fastjson2.JSONObject;
import lombok.Data;

import java.util.List;

/**
 * 发送消息mqDTO
 *
 * @author chenHui
 * @version 1.0.0
 */
@Data
public class MsgSendDTO {
    /**
     * 消息模板编码
     */
    private String msgCode;
    /**
     * 消息接收人
     */
    private List<String> toUser;
    /**
     * 抄送人员
     */
    private List<String> cc;

    /**
     * 模板动态参数
     */
    private JSONObject param;

    /**
     * 附件
     */
    private String attachment;

    /**
     * 系统id
     */
    private String appId;

    private String title;

    private String content;

    private String remark;

}
