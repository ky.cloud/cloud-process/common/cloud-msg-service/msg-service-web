package com.ky.cloud.msg;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.ky.cloud.msg.mapper")
@SpringBootApplication(scanBasePackages = {"com.ky.cloud.**"})
public class MsgServiceWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsgServiceWebApplication.class, args);
    }
}
