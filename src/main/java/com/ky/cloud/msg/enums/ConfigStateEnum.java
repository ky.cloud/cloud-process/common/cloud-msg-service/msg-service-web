package com.ky.cloud.msg.enums;

/**
 * @author chenhui_sinosoft
 * @version 1.0.0
 */
public enum ConfigStateEnum {
    APPLY("0","申请"),
    ENABLE("1","启用"),
    STOP("-1","停用");

    private String state;

    private String desc;

    ConfigStateEnum(String state, String desc) {
        this.state = state;
        this.desc = desc;
    }

    public String getState() {
        return state;
    }

    public String getDesc() {
        return desc;
    }
}
