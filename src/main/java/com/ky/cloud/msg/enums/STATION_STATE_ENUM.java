package com.ky.cloud.msg.enums;

/**
 * @author nfc
 * @version 1.0.0
 */
public enum STATION_STATE_ENUM {

    READ_STATE("0", "已读"),
    UNREAD_STATE("1", "未读"),
    READING_STATE("2", "读取中");

    private final String code;
    private final String desc;

    STATION_STATE_ENUM(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
