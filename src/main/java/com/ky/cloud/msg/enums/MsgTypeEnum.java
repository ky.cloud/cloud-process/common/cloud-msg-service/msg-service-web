package com.ky.cloud.msg.enums;

/**
 * @author chenHui
 * @version 1.0.0
 */
public enum MsgTypeEnum {
    SMS("sms","短信"),
    MAIL("mail","邮箱"),
    STATION_MSG("stationMsg","站内消息");

    private final String type;

    private final String desc;

    MsgTypeEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public String getDesc() {
        return desc;
    }
}
