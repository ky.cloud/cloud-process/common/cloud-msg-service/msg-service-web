package com.ky.cloud.msg.utils;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import org.apache.commons.text.StringSubstitutor;

import java.util.HashMap;
import java.util.Map;

/**
 * string工具类
 *
 * @author nfc
 * @version 1.0.0
 */
public class StringUtils {

    public static String replaceOfString(Object param, String msgContent) {
        @SuppressWarnings("ALL")
        Map<String, String> paramsMap = JSONObject.parseObject(JSON.toJSONString(param), HashMap.class);
        StringSubstitutor sub = new StringSubstitutor(paramsMap);
        return sub.replace(msgContent);
    }


}
