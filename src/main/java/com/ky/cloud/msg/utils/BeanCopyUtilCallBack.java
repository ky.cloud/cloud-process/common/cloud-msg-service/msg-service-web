package com.ky.cloud.msg.utils;

/**
 * copy回调
 *
 * @author Lv Xun
 * @date 15:18 2021/7/2
 * @since 1.0
 */
public interface BeanCopyUtilCallBack<S, T> {
    /**
     * 定义默认回调方法
     *
     * @param t
     * @param s
     */
    void callBack(S t, T s);
}
