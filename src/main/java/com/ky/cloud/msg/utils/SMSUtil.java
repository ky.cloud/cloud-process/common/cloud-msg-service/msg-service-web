package com.ky.cloud.msg.utils;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson2.JSONObject;
import com.ky.cloud.components.redis.util.RedisUtils;
import com.ky.cloud.msg.mq.receive.MsgSendMqDTO;
import com.tencentcloudapi.common.AbstractModel;
import com.tencentcloudapi.common.Credential;
import com.tencentcloudapi.common.exception.TencentCloudSDKException;
import com.tencentcloudapi.common.profile.ClientProfile;
import com.tencentcloudapi.common.profile.HttpProfile;
import com.tencentcloudapi.sms.v20190711.SmsClient;
import com.tencentcloudapi.sms.v20190711.models.SendSmsRequest;
import com.tencentcloudapi.sms.v20190711.models.SendSmsResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Component
public class SMSUtil {
    @Value("${sms.sdkAppId}")
    private String sdkAppId;
    @Value("${sms.signName}")
    private String signName;

    @Value("${sms.timeout}")
    private String timeout;

    @Value("${tencent.accessKeyId}")
    private String accessKeyId;
    @Value("${tencent.secretAccessKey}")
    private String secretAccessKey;

    @Resource
    private RedisUtils redisUtils;

    /**
     * 发送短信（腾讯云）
     *
     * @param messageDTO MessageDTO
     * @return boolean
     * @since 1.0.0
     */
    public boolean sendSms(MsgSendMqDTO messageDTO) {
        SendSmsResponse res;
        JSONObject param = messageDTO.getParam();

        SendSmsRequest req = new SendSmsRequest();
        try {

            Credential cred = new Credential(accessKeyId, secretAccessKey);
            // 实例化一个http选项，可选，没有特殊需求可以跳过
            HttpProfile httpProfile = new HttpProfile();
            // 设置代理 SDK默认使用POST方法,如果你一定要使用GET方法，可以在这里设置。GET方法无法处理一些较大的请求
            httpProfile.setReqMethod("POST");
            // SDK有默认的超时时间，非必要请不要进行调整 如有需要请在代码中查阅以获取最新的默认值
            httpProfile.setConnTimeout(60);
            // SDK会自动指定域名。通常是不需要特地指定域名的，但是如果你访问的是金融区的服务 则必须手动指定域名，例如sms的上海金融区域名： sms.ap-shanghai-fsi.tencentcloudapi.com
            httpProfile.setEndpoint("sms.tencentcloudapi.com");

            ClientProfile clientProfile = new ClientProfile();
            // SDK默认用TC3-HMAC-SHA256进行签名，非必要请不要修改这个字段
            clientProfile.setSignMethod("HmacSHA256");
            clientProfile.setHttpProfile(httpProfile);
            // 实例化要请求产品(以sms为例)的client对象,第二个参数是地域信息，可以直接填写字符串ap-guangzhou，或者引用预设的常量
            SmsClient client = new SmsClient(cred, "ap-guangzhou", clientProfile);


            /* 填充请求参数,这里request对象的成员变量即对应接口的入参
             * 你可以通过官网接口文档或跳转到request对象的定义处查看请求参数的定义
             * 基本类型的设置:
             * 帮助链接：
             * 短信控制台: https://console.cloud.tencent.com/smsv2
             * sms helper: https://cloud.tencent.com/document/product/382/3773 */

            /* 短信应用ID: 短信SdkAppId在 [短信控制台] 添加应用后生成的实际SdkAppId，示例如1400006666 */
            req.setSmsSdkAppid(sdkAppId);

            /* 短信签名内容: 使用 UTF-8 编码，必须填写已审核通过的签名，签名信息可登录 [短信控制台] 查看 */
            req.setSign(signName);

            /* 国际/港澳台短信 SenderId: 国内短信填空，默认未开通，如需开通请联系 [sms helper] */
            String senderid = "";
            req.setSenderId(senderid);

            /* 短信号码扩展号: 默认未开通，如需开通请联系 [sms helper] */
            String extendCode = "";
            req.setExtendCode(extendCode);

            /* 模板 ID: 必须填写已审核通过的模板 ID。模板ID可登录 [短信控制台] 查看 */
            req.setTemplateID(messageDTO.getRemark());

            /* 下发手机号码，采用 E.164 标准，+[国家或地区码][手机号]
             * 示例如：+8613711112222， 其中前面有一个+号 ，86为国家码，13711112222为手机号，最多不要超过200个手机号 */
            List<String> toUser = messageDTO.getToUser();
            String[] phoneNumberSet = new String[toUser.size()];
            int i=0;
            for (String mobile : toUser) {
                phoneNumberSet[i] = "+86"+mobile;
                i++;
            }
            req.setPhoneNumberSet(phoneNumberSet);

            String code = RandomUtil.randomNumbers(6);
            redisUtils.set("code." + timeout, code, Integer.parseInt(timeout) * 60L);
            String[] templateParamSet = {code, timeout};
            req.setTemplateParamSet(templateParamSet);
            /* 模板参数: 若无模板参数，则设置为空 */
//            if (!StringUtils.hasText(req.getTemplateID())) {
//                String[] templateParamSet = {param.getString("p1"), param.getString("p2"), param.getString("p3"), param.getString("p5")};
//                req.setTemplateParamSet(templateParamSet);
//            }
            //将验证码放入缓存
            /* 通过 client 对象调用 SendSms 方法发起请求。注意请求方法名与请求对象是对应的
             * 返回的 res 是一个 SendSmsResponse 类的实例，与请求对象对应 */
            res = client.SendSms(req);

            // 输出json格式的字符串回包
            log.info(AbstractModel.toJsonString(res));
            // 也可以取出单个值，你可以通过官网接口文档或跳转到response对象的定义处查看返回字段的定义
            log.info(res.getRequestId());

        } catch (TencentCloudSDKException e) {
            e.printStackTrace();
            log.error("发送给短信失败！当前请求示例对象为:" + AbstractModel.toJsonString(req));
            return false;
        }
        return true;
    }
}
