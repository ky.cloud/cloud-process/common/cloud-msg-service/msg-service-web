package com.ky.cloud.msg.utils;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ky.cloud.msg.exception.MsgException;
import com.ky.cloud.msg.mq.receive.MsgSendMqDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.annotation.Resource;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.List;

/**
 * @author chenHui
 * @version 1.0.0
 */
@Slf4j
@Component
public class MailUtil {
    @Resource
    private JavaMailSenderImpl javaMailSender;
    @Value("${spring.mail.username}")
    private String mailUser;
    @Resource
    private TemplateEngine templateEngine;

    public boolean sendMail(MsgSendMqDTO msgSendMqDTO) {
        try {
            MimeMessage mailMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mailMessage, true);
            mimeMessageHelper.setFrom(mailUser);
            List<String> toUserList = msgSendMqDTO.getToUser();
            String[] toUser = toUserList.toArray(new String[0]);
            mimeMessageHelper.setTo(toUser);
            List<String> ccList = msgSendMqDTO.getCc();
            if (!CollectionUtils.isEmpty(ccList)) {
                String[] toCC = ccList.toArray(new String[0]);
                mimeMessageHelper.setCc(toCC);
            }
            mimeMessageHelper.setSubject(msgSendMqDTO.getTitle());
            String content = buildContent(msgSendMqDTO);
            mimeMessageHelper.setText(content, true);
            mimeMessageHelper.setSentDate(new Date());
            javaMailSender.send(mimeMessageHelper.getMimeMessage());
            templateEngine.clearTemplateCacheFor(msgSendMqDTO.getMsgCode());
            return true;
        } catch (MessagingException e) {
            log.error("邮件发送异常\n参数:{}\n", JSON.toJSONString(msgSendMqDTO), e);
            return false;
        } catch (MsgException e) {
            return false;
        }
    }

    /**
     * <p>读取自定义HTML模板目录</p>
     *
     * @return 返回解析后的邮件内容
     */
    public String buildContent(MsgSendMqDTO msgSendMqDTO) throws MsgException {
        try {
            Context context = new Context();
            JSONObject param = msgSendMqDTO.getParam();
            for (String key : param.keySet()) {
                context.setVariable(key, param.get(key));
            }
            return templateEngine.process(msgSendMqDTO.getMsgCode(), context);
        } catch (Exception e) {
            log.error("数据填充模板异常，原因{}",e.getMessage());
            throw new MsgException("数据填充模板异常");
        }
    }
}
