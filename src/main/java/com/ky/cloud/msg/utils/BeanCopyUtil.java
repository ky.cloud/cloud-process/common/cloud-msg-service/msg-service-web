package com.ky.cloud.msg.utils;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

/**
 * beanCopy工具类
 *
 * @author chenHui
 * @version 1.0.0
 */
public class BeanCopyUtil extends BeanUtils {
    /**
     * 集合数据的拷贝
     *
     * @param sources: 数据源类
     * @param target:  目标类::new(eg: UserVO::new)
     * @return 拷贝完成数据集合
     */
    public static <S, T> List<T> copyListProperties(List<S> sources, Supplier<T> target) {
        return copyListProperties(sources, target, null);
    }

    /**
     * 带回调函数的集合数据的拷贝（可自定义字段拷贝规则）
     *
     * @param sources:  数据源类
     * @param target:   目标类::new(eg: UserVO::new)
     * @param callBack: 回调函数
     * @return 拷贝完成数据集合
     */
    public static <S, T> List<T> copyListProperties(List<S> sources, Supplier<T> target, BeanCopyUtilCallBack<S, T> callBack) {
        List<T> list = new ArrayList<>(sources.size());
        for (S source : sources) {
            T t = target.get();
            copyProperties(source, t);
            list.add(t);
            if (callBack != null) {
                // 回调
                callBack.callBack(source, t);
            }
        }
        return list;
    }

    /**
     * 把oldJavaBean非null的值映射到newJavaBean上
     *
     * @param oldJavaBean 有值的javaBean
     * @param newJavaBean 需要填充值的javaBean
     * @author chenHui
     * @since 1.0.0
     */
    public static void copyPropertiesNotNull(Object oldJavaBean, Object newJavaBean) {
        BeanUtils.copyProperties(oldJavaBean, newJavaBean, getNullPropertyNames(oldJavaBean));
    }

    /**
     * 把oldJavaBean值映射到newJavaBean为null的值上
     *
     * @param oldJavaBean 有值的javaBean
     * @param newJavaBean 需要填充值的javaBean
     * @author chenHui
     * @date 2020/12/25 11:20
     */
    public static void copyPropertiesNewNull(Object oldJavaBean, Object newJavaBean) {
        BeanUtils.copyProperties(oldJavaBean, newJavaBean, getSamePropertyNames(newJavaBean));
    }

    private static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for (PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }


    /**
     * 获取javaBean有值的字段集合
     *
     * @param source 需要填充值的javaBean
     * @return 有值的javaBean
     */
    private static String[] getSamePropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<>();
        for (PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue != null) {
                emptyNames.add(pd.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }


}
