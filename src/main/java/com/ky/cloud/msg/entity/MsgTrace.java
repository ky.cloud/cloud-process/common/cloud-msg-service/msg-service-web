package com.ky.cloud.msg.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 消息轨迹表
 * </p>
 *
 * @author author
 * @since 2023-07-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("msg_trace")
public class MsgTrace extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 轨迹id
     */
    @TableId(value = "TRACE_ID", type = IdType.AUTO)
    private Integer traceId;

    /**
     * 消息数据
     */
    @TableField("MSG_DATA")
    private String msgData;

    /**
     * 状态
     */
    @TableField("STATE")
    private String state;

    /**
     * 错误信息
     */
    @TableField("ERROR_MSG")
    private String errorMsg;

    /**
     * 重试次数
     */
    @TableField("RETRY_COUNT")
    private Integer retryCount;

    /**
     * 发送时间
     */
    @TableField("SEND_TIME")
    private Date sendTime;

    /**
     * 系统id
     */
    @TableField("APP_ID")
    private String appId;

    /**
     * 消息编码
     */
    @TableField("MSG_CODE")
    private String msgCode;

    /**
     * 站内消息
     */
    @TableField("MSG")
    private String msg;


}
