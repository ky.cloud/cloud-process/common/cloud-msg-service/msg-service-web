package com.ky.cloud.msg.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ky.cloud.msg.entity.BaseEntity;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 消息配置表
 * </p>
 *
 * @author Administrator
 * @since 2023-06-09
 */
@Getter
@Setter
@TableName("msg_config")
public class MsgConfig extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 消息主键
     */
    @TableId(value = "MSG_CONFIG_ID", type = IdType.AUTO)
    private Integer msgConfigId;

    /**
     * 消息编码
     */
    @TableField("MSG_CODE")
    private String msgCode;

    /**
     * 消息类型
     */
    @TableField("TYPE")
    private String type;

    /**
     * 消息用途
     */
    @TableField("PURPOSES")
    private String purposes;

    /**
     * 消息主题
     */
    @TableField("MSG_TITLE")
    private String msgTitle;

    /**
     * 消息内容
     */
    @TableField("MSG_CONTENT")
    private String msgContent;

    /**
     * 消息附件
     */
    @TableField("ATTACHMENT")
    private String attachment;

    /**
     * 系统id
     */
    @TableField("APP_ID")
    private String appId;

    /**
     * 系统环境
     */
    @TableField("EVN")
    private String evn;

    /**
     * 状态
     */
    @TableField("STATE")
    private String state;

    /**
     * 备注
     */
    @TableField("REMARK")
    private String remark;
}
