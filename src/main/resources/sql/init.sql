DROP TABLE IF EXISTS msg_config;
CREATE TABLE msg_config
(
    `MSG_CONFIG_ID` INT(8)       NOT NULL AUTO_INCREMENT COMMENT '消息主键',
    `MSG_CODE`      VARCHAR(20) COMMENT '消息编码',
    `TYPE`          VARCHAR(10)  NOT NULL COMMENT '消息类型',
    `PURPOSES`      VARCHAR(100) NOT NULL COMMENT '消息用途',
    `MSG_TITLE`     VARCHAR(20) COMMENT '消息主题',
    `MSG_CONTENT`   text COMMENT '消息内容',
    `CONTENT_STYLE` VARCHAR(10)  NOT NULL COMMENT '消息内容格式',
    `ATTACHMENT`    json COMMENT '消息附件',
    `APP_ID`        VARCHAR(20)  NOT NULL COMMENT '系统id',
    `STATE`         VARCHAR(10)  NOT NULL COMMENT '状态',
    `REMARK`        text COMMENT '备注',
    `CREATE_BY`     VARCHAR(20)  NOT NULL COMMENT '创建人',
    `CREATE_TIME`   DATETIME     NOT NULL COMMENT '创建时间',
    `UPDATE_BY`     VARCHAR(20)  NOT NULL COMMENT '更新人',
    `UPDATE_TIME`   DATETIME     NOT NULL COMMENT '更新时间',
    `IS_DELETE`     tinyint(1)   NOT NULL default 0 COMMENT '是否删除',
    PRIMARY KEY (MSG_CONFIG_ID)
) COMMENT = '消息配置表';


DROP TABLE IF EXISTS msg_trace;
CREATE TABLE msg_trace
(
    `TRACE_ID`    INT(8)      NOT NULL AUTO_INCREMENT COMMENT '轨迹id',
    `MSG_DATA`    json        NOT NULL COMMENT '消息数据',
    `STATE`       varchar(10) NOT NULL COMMENT '状态',
    `ERROR_MSG`   VARCHAR(100) COMMENT '错误信息',
    `RETRY_COUNT` INT(1) default 0 COMMENT '重试次数',
    `SEND_TIME`   DATETIME COMMENT '发送时间',
    `APP_ID`      varchar(20) NOT NULL COMMENT '系统id',
    `MSG_CODE`    varchar(20) NOT NULL COMMENT '消息编码',
    `CREATE_BY`   VARCHAR(20) NOT NULL COMMENT '创建人',
    `CREATE_TIME` DATETIME    NOT NULL COMMENT '创建时间',
    `UPDATE_BY`   VARCHAR(20) NOT NULL COMMENT '更新人',
    `UPDATE_TIME` DATETIME    NOT NULL COMMENT '更新时间',
    `IS_DELETE`   tinyint(1)  NOT NULL COMMENT '是否删除',
    PRIMARY KEY (TRACE_ID)
) COMMENT = '消息轨迹表';
