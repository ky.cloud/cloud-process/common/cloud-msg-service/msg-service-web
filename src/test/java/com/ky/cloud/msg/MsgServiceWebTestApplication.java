package com.ky.cloud.msg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MsgServiceWebTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsgServiceWebTestApplication.class, args);
    }
}
